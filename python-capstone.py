from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass
    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department


    # Getters
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName
    
    def get_email(self):
        return self._email

    def get_department(self):
        return self._department

    # Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName =  lastName
    
    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

    
    # Abstract methods
    def getFullName(self):
        return self._firstName + " " + self._lastName
    
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"
    
    def addRequest(self):
        return "Request has been added"

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []


    # Getters
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName
    
    def get_email(self):
        return self._email

    def get_department(self):
        return self._department
    
    def get_members(self):
        return self._members

    # Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName =  lastName
    
    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

    
    # Abstract methods
    def getFullName(self):
        return self._firstName + " " + self._lastName
    
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def addMember(self, employee):
        print(f"{employee.getFullName()} has been added")
        self._members.append(employee)
        return self._members

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department


    # Getters
    def get_firstName(self):
        return self._firstName

    def get_lastName(self):
        return self._lastName
    
    def get_email(self):
        return self._email

    def get_department(self):
        return self._department

    # Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName =  lastName
    
    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

    
    # Abstract methods
    def getFullName(self):
        return self._firstName + " " + self._lastName
    
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"
    
    def addUser(self):
        return "User has been added"

class Request():
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "open"

    # setter
    def set_name(self, name):
        self._name = name

    def set_requester(self, requester):
        self._requester = requester

    def set_dateRequested(self, dateRequested):
        self._dateRequested = dateRequested
        
    def set_status(self, status):
        self._status = status

    # getter
    def get_name(self):
        return self._name

    def get_requester(self):
        return self._requester
   
    def get_dateRequested(self):
        return self._dateRequested
   
    def get_status(self):
        return self._status

    # Methods
    def updateRequest(self, name):
        self._name = name
        return self._name

    def closeRequest(self):
        self._status = "closed"
        return f"Request {self._name} has been {self._status}."

    def cancel_request(self):
        self._status = "cancelled"
        return f"Request {self._name} has been {self._status}."
    



employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamlead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamlead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamlead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamlead1.addMember(employee3)
teamlead1.addMember(employee4)
for members in teamlead1.get_members():
    print(members.getFullName())

assert admin1.addUser() == "User has been added"
print(req2.closeRequest())
print(req1.cancel_request())